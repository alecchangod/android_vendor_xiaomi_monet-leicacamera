
BUILD_BROKEN_DUP_RULES := true

TARGET_CAMERA_USES_NEWER_HIDL_OVERRIDE_FORMAT := true

PRODUCT_SOONG_NAMESPACES += \
	$(LOCAL_PATH)

# Overlay
PRODUCT_PACKAGES += \
    MiuiCameraOverlayLos \
    MiuiCameraOverlayAosp

# Properties
TARGET_SYSTEM_PROP += vendor/xiaomi/monet-leicacamera/configs/props/system.prop
TARGET_VENDOR_PROP += vendor/xiaomi/monet-leicacamera/configs/props/vendor.prop

# Sepolicy
BOARD_VENDOR_SEPOLICY_DIRS += vendor/xiaomi/monet-leicacamera/sepolicy/vendor

# Copy files
PRODUCT_COPY_FILES += \
    vendor/xiaomi/monet-leicacamera/proprietary/vendor/etc/camera/sceneDetection.xml:$(TARGET_COPY_OUT_VENDOR)/etc/camera/sceneDetection.xml

# Permissions
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/xiaomi/monet-leicacamera/proprietary/system/etc/permissions,$(TARGET_COPY_OUT_SYSTEM)/etc/permissions)

# sysconfig
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/xiaomi/monet-leicacamera/proprietary/system/etc/sysconfig,$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig)

# libs
# Camera
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/xiaomi/monet-leicacamera/proprietary/system/priv-app/MiuiCamera/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib)

# MiuiExtraPhoto
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/xiaomi/monet-leicacamera/proprietary/system/priv-app/MiuiExtraPhoto/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiExtraPhoto/lib)

# Packages
PRODUCT_PACKAGES += \
	MiuiCamera \
	MiuiScanner \
	MiuiExtraPhoto